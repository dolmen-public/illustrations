# Illustrations

Repository of home-made vectorial illustrations.

## [Contributors](./contributors.md)

## [Licence under GPLv3](./LICENSE)

You may contact [Savadenn](https://www.savadenn.bzh) if you wish to acquire another licence.
